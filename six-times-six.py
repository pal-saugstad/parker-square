#!/usr/bin/python


"""MPMP: Unique Distancing Problem

By: Pal Saugstad (https://bitbucket.org/pal-saugstad/parker-square)
Email: pal@saugstad.net

How to place 6 pieces on a 6x6 board where all distances between the pieces are unique.
Please watch https://www.youtube.com/watch?v=M_YOCQaI5QI for more info regarding the puzzle.

"""


import time
import sys
import argparse
import hashlib

expected_signatures = {
    0: 'd41d8cd98f00b204e9800998ecf8427e',
    1: 'd41d8cd98f00b204e9800998ecf8427e',
    2: 'c51ce410c124a10e0db5e4b97fc2af39',
    3: 'afef09590f27bd942b1d49259f9baaad',
    4: '03dff54ec27555a678f755e8f614219e',
    5: '2adf8c4e0bb3728473feabb151524daf',
    6: '263bb359bc705c6a7e93f64de1b44bbb',
    7: 'a18d993c813df66616bb0b86b4851bad',
    8: 'd41d8cd98f00b204e9800998ecf8427e',
    9: 'd41d8cd98f00b204e9800998ecf8427e',
    10: 'd41d8cd98f00b204e9800998ecf8427e'
}
signatures = {}  # {[,,],[,,]}
SIGNATURE = 0
SOLUTIONS = 1
PIECES = 2
TIME = 3
IS_OK = 4

debug = False
debug2 = False


def make_number_of(board_list):
    board_list.sort()
    ret_val = 0
    for num in board_list:
        ret_val *= square_size
        ret_val += num
    return ret_val


def print_board_list(text, board_list):
    board = [0]*square_size
    for num in board_list:
        board[num] = 1
    if not args.q:
        print text
        for i in range(side):
            print_board_line_nice(i*side, board)


def print_board_from_num(text, num):
    board_list = []
    values = pieces
    while values:
        board_list.append(num % square_size)
        num //= square_size
        values -= 1
    print_board_list(text, board_list)


def dist_sign(board_list):
    sign = []
    for k in board_list:
        inner = []
        for l in board_list:
            if k != l:
                inner.append(sqr_dist(k, l))
        inner.sort()
        sign.append('-'.join(map(str, inner)))
    sign.sort()
    return ' '.join(sign)


def board_append(this_board):
    global unique_indicator
    global glob_uniq_boards
    dist_id = dist_sign(this_board)
    num = make_number_of(this_board)
    is_unique = 'Uniq  --'
    if dist_id not in glob_uniq_board_ids:
        unique_indicator += 1
        is_unique = 'Uniq{:4d}'.format(unique_indicator)
        glob_uniq_boards.append(num)
        glob_uniq_board_ids[dist_id] = 1
    if debug2:
        print "dist_id", this_board, dist_id, is_unique


def print_board_line(i, board):
    lformat = ''
    for j in range(side):
        val = board[i+j]
        char = '.'
        if val == -1:
            char = ' '
        elif val >= 0:
            char = str(val)
        lformat += "{:3s}".format(char)
    print lformat


def print_board_line_nice(i, board):
    lformat = ''
    for j in range(side):
        lformat += "{:3s}".format('.' if board[i+j] <= 0 else "o")
    print lformat


def print_board_debug(piece, board):
    print "{}x{}: Unique solutions so far: {} Max pieces placed: {}".format(side, side, unique_indicator, maxpieces)

    for i in range(side):
        print_board_line(i*side, board)
    time.sleep(.07)


def sqr_dist(idx, jdx):
    if idx > jdx:
        if jdx % side < idx % side:
            return mb[idx - jdx]
        return ml[idx - jdx]
    if idx % side < jdx % side:
        return mb[jdx - idx]
    return ml[jdx - idx]


def sqr_dist_slow(idx, jdx):
    r = idx // side
    c = idx % side
    R = jdx // side
    C = jdx % side
    h = R - r
    v = C - c
    return h * h + v * v


def update_dists(idx, board_pos, dists):
    for jdx in board_pos:
        m = sqr_dist(idx, jdx)
        if debug:
            print "Register [{},{}] is sqrt({:3d}) away from [{},{}]".format(
                   idx // side, idx % side, m, jdx // side, jdx % side)
        assert m not in dists
        dists[m] = 1
    return


def check_dists(idx, board_pos, dists):
    inter_dists = {}
    for jdx in board_pos:
        m = sqr_dist(idx, jdx)
        if debug:
            print "Check: [{},{}] is sqrt({:3d}) away from [{},{}]".format(
                  idx // side, idx % side, m, jdx // side, jdx % side), '',
        if m in dists:
            if debug:
                print "- Used"
            return True
        if m in inter_dists:
            if debug:
                print "- Twice"
            return True
        inter_dists[m] = 1
        if debug:
            print ""
    return False


def find_unusable_squares(board, board_pos, dists):
    if debug:
        print "find_unusable_squares - board:", board, "board_pos:", board_pos, "dists:", dists
    for idx in range(square_size):
        if board[idx] == 0:
            # test if this square has a distance to any of the other pieces which has already been used
            if check_dists(idx, board_pos, dists):
                board[idx] = -2


def level(idx, board, board_pos, dists):
    global t_rep, maxpieces
    # piece: the piece to place now (1 to 6)
    # idx: the first possible position this piece can occupy
    # board: the board up to this point
    # dists: the coded dists array
    if idx >= square_size:
        return True
    piece = len(board_pos) + 1
    if piece > pieces:
        return True
    while idx < (square_size - pieces + piece - 1):
        idx += 1
        if board[idx] == 0:
            this_board = list(board)
            this_board_pos = list(board_pos)
            this_dists = dists.copy()

            # Register new used dists due to this piece
            update_dists(idx, this_board_pos, this_dists)
            this_board[idx] = piece
            this_board_pos.append(idx)
            if piece >= pieces:
                board_append(this_board_pos)
            # Mark unusable squares with negative values
            find_unusable_squares(this_board, this_board_pos, this_dists)
            if piece > maxpieces:
                maxpieces = piece
            if debug:
                print "---- Piece:", piece
                print "board:", this_board
                print "board_pos:", this_board_pos
                print "dists:", this_dists
                print_board_debug(piece, this_board)
            if time.clock() > t_rep:
                print "{}x{} Max:{:3d}".format(side, side, maxpieces), board_pos
                t_rep += 10.0
            level(idx, this_board, this_board_pos, this_dists)
            del this_dists
            del this_board
            del this_board_pos
    return


desc = "How to place n pieces on a RxR board so that all distances between pieces are different"
parser = argparse.ArgumentParser(description=desc)
parser.add_argument("-r", default=6, type=int, help="Number of rows and columns (R)")
parser.add_argument("-a", default=False, action='store_true', help="Calculate all boards below the chosen")
parser.add_argument("-q", default=False, action='store_true', help="Be quiet")
parser.add_argument("-d", default=False, action='store_true', help="Debug output")
parser.add_argument("-D", default=False, action='store_true', help="More debug output")
args = parser.parse_args()
debug = args.d
debug2 = args.D
stop_side = args.r if args.r >= 2 else 2
start_side = 2 if args.a else stop_side

t_rep = time.clock() + 10.0
for side in range(start_side, stop_side+1):
    board_no = 0
    unique_indicator = 0
    pieces = side
    t0 = time.clock()
    maxpieces = 0
    glob_uniq_boards = []
    glob_uniq_board_ids = {}
    square_size = side * side

    # Assuming here: a 6x6 board

    # The board square is represented in three ways,
    #     as 'board'. an list running from 0 to square_size-1
    #     and as coordinates, so that if idx = 6, r = 1 and c = 0
    #     and as 'board_pos', a list of the positions in ascending order

    # board = [1,0,0,0,0,0,
    #          2,0,0,0,0,0,
    #          0,3,0,0,0,0,
    #          0,0,0,0,0,0,
    #          0,0,0,0,0,0,
    #          0,0,0,0,0,0]

    # board_pos = [0, 6, 13] corresponds to the board above

    # dists format: square distances
    # (so that the 'Manhattan' distance [3,4] is 25,
    # same as two points in a straight line with real distance 5)
    # The value is 1 always (So the information is in the key, not the value)
    # So
    # dists = {1: 1, 5: 1, 2: 1}
    #    for the 6x6 board above

    # build board
    init_board = [0]*square_size
    dists = {}

    # init sqr_dist accelerator
    mb = []  # when biggest index is to the right of smallest index
    ml = []  # when biggest index is to the left of smallest index

    for jdx in range(square_size):
        mb.append(sqr_dist_slow(0,jdx))
        ml.append(sqr_dist_slow(side - 1,side - 1 + jdx))

    for idx in range(square_size):
        for jdx in range(square_size):
            assert sqr_dist_slow(idx, jdx) == sqr_dist(idx, jdx)

    for p in range((side + 1) // 2):
        if init_board[p]:
            continue
        board = list(init_board)
        board[p] = 1
        board_pos = [p]
        dists = {}
        if debug2:
            print "First piece in [{},{}]".format(p // side, p % side)
        level(p, board, board_pos, dists)
        del board
        del board_pos
        del dists
        r = p // side
        c = p % side
        # Mark any symetrical position to the current as already checked
        init_board[r * side + c] = -1
        init_board[c * side + r] = -1
        init_board[(side - 1 - r) * side + c] = -1
        init_board[(side - 1 - c) * side + r] = -1
        init_board[(side - 1 - r) + side * c] = -1
        init_board[(side - 1 - c) + side * r] = -1
        init_board[(side - 1 - r) + side * (side - 1 - c)] = -1
        init_board[(side - 1 - c) + side * (side - 1 - r)] = -1

    if debug2:
        print glob_uniq_boards
        print glob_uniq_board_ids
    t1 = time.clock()
    dashes = ''
    for i in range(side - 1):
        dashes += '---'
    board_no = 0
    md5 = hashlib.md5()
    if not args.q:
        print "\nHow to place {} pieces on a {}x{} board so that all distances between pieces are different (-h for help)\n".format(
              pieces, side, side)
    for num in glob_uniq_boards:
        md5.update(str(num))
        board_no += 1
        same_as = ''
        if not args.q:
            print_board_from_num("{}-  {}{}".format(dashes, board_no, same_as), num)
    if not args.q:
        print "{}-\n".format(dashes)

    this_signature = [0, 0, 0, 0.0, False]
    this_signature[SIGNATURE] = md5.hexdigest()
    this_signature[SOLUTIONS] = board_no
    this_signature[PIECES] = pieces
    this_signature[TIME] = t1 - t0
    del md5
    signatures[side] = this_signature
    if side not in expected_signatures:
        break
    elif expected_signatures[side] != this_signature[SIGNATURE]:
        break

for i in signatures:
    this_signature = signatures[i]
    if i not in expected_signatures:
        print "Signature for {}x{}: {}".format(side, side, this_signature[SIGNATURE])
    elif expected_signatures[i] != this_signature[SIGNATURE]:
        print "Something odd happened: For {}x{}, signature was {}, not {}".format(
               side, side, this_signature[SIGNATURE], expected_signatures[i])
    else:
        signatures[i][IS_OK] = True

print ''
for i in signatures:
    this_signature = signatures[i]
    print "Found{:3d} unique solutions for {} pieces on a {}x{} board in{:8.3f} seconds".format(
          this_signature[SOLUTIONS],
          this_signature[PIECES], i, i,
          this_signature[TIME])
    if not this_signature[IS_OK]:
        print '\nERROR!!! Signature FAILED for the {}x{} board\n'.format(i, i)
        sys.exit(1)
print ''
sys.exit(0)
