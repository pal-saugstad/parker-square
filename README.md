# MPMP: Unique Distancing Problem #

How to place 6 pieces on a 6x6 board where all distances between the pieces are unique.

### Python scipt which prints solutions of this puzzle ###

Here is the puzzle solved for a RxR board where R shouldn't be set lower than 2. Set it higher than 8 only if you have lots of time...

Please watch
[this](https://www.youtube.com/watch?v=M_YOCQaI5QI "The Youtube Puzzle Video - MPMP: Unique Distancing Problem")
Youtube video for detailed information about the puzzle.
This is a video in the series "Matt Parker's Maths Puzzles"

### How to run ###

`python six-times-six.py [-h]`

### Standard option ###

-r <rows> (default 6): Number of rows and columns on the board

### Other options ###

Please run `python six-times-six.py -h` for more information

### Rough speed estimates ###

On my laptop, I got these results:

```
$ python six-times-six.py -qar 9

Found  2 unique solutions for 2 pieces on a 2x2 board in   0.000 seconds
Found  3 unique solutions for 3 pieces on a 3x3 board in   0.000 seconds
Found 16 unique solutions for 4 pieces on a 4x4 board in   0.003 seconds
Found 28 unique solutions for 5 pieces on a 5x5 board in   0.019 seconds
Found  2 unique solutions for 6 pieces on a 6x6 board in   0.145 seconds
Found  1 unique solutions for 7 pieces on a 7x7 board in   1.245 seconds
Found  0 unique solutions for 8 pieces on a 8x8 board in  11.128 seconds
Found  0 unique solutions for 9 pieces on a 9x9 board in  80.574 seconds
```

### Number of solutions ###

In this version, I am using "square distances",
so that any solution with three pieces arranged
as a right angled triangle with distances 3, 4 and 5
will be eliminated
if there are other pieces forming the distance 5 as a straight line.
This reduces the number of solutions greatly for boards larger than 5x5!
